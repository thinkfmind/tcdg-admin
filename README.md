# TCDG - ADMIN

## Requisites
- npm

## Installation
**RUN:** `npm install`

## Start Deployment:
**RUN:** `npm start`

## Production:
**RUN:** `npm run build`
this create a build directory. you can copy this directory in your server web folder and acces to index.html for run the app.

## Documentation
The documentation for the Material Dashboard React is hosted at our [website](https://demos.creative-tim.com/material-dashboard-react/#/documentation/tutorial).


## File Structure

Within the download you'll find the following directories and files:

```
material-dashboard-react
.
├── CHANGELOG.md
├── ISSUE_TEMPLATE.md
├── LICENSE.md
├── README.md
├── bower.json
├── gulpfile.js
├── jsconfig.json
├── package.json
├── documentation
│   ├── assets
│   │   ├── css
│   │   ├── img
│   │   │   └── faces
│   │   └── js
│   └── tutorial-components.html
├── public
│   ├── favicon.ico
│   ├── index.html
│   └── manifest.json
└── src
    ├── index.js
    ├── logo.svg
    ├── routes.js
    ├── assets
    │   ├── css
    │   │   └── material-dashboard-react.css
    │   ├── github
    │   │   ├── md-react.gif
    │   │   └── react.svg
    │   ├── img
    │   │   └── faces
    │   └── jss
    │       ├── material-dashboard-react
    │       │   ├── components
    │       │   ├── layouts
    │       │   └── views
    │       └── material-dashboard-react.js
    ├── components
    │   ├── Card
    │   │   ├── Card.js
    │   │   ├── CardAvatar.js
    │   │   ├── CardBody.js
    │   │   ├── CardFooter.js
    │   │   ├── CardHeader.js
    │   │   └── CardIcon.js
    │   ├── CustomButtons
    │   │   └── Button.js
    │   │   └── ProgressButton.js
    │   ├── CustomInput
    │   │   └── AyncAutocomplete.js
    │   │   └── CustomCheckbox.js
    │   │   └── CustomColorPicker.js
    │   │   └── CustomDatePicker.js
    │   │   └── CustomDateTimePicker.js
    │   │   └── CustomInput.js
    │   │   └── CustomInputFile.js
    │   │   └── CustomSelect.js
    │   │   └── CustomTextArea.js
    │   │   └── FormCheckbox.js
    │   │   └── SimpleInput.js
    │   ├── CustomTabs
    │   │   └── CustomTabs.js
    │   ├── FixedPlugin
    │   │   └── FixedPlugin.js
    │   ├── Footer
    │   │   └── Footer.js
    │   ├── Grid
    │   │   ├── GridContainer.js
    │   │   └── GridItem.js
    │   ├── Navbars
    │   │   ├── AdminNavbarLinks.js
    │   │   ├── Navbar.js
    │   │   ├── Pagination.js
    │   │   └── Searchbar.js
    │   ├── Sidebar
    │   │   └── Sidebar.js
    │   ├── Snackbar
    │   │   ├── Snackbar.js
    │   │   └── SnackbarContent.js
    │   ├── Table
    │   │   ├── CollapseRow.js
    │   │   ├── Table.js
    │   │   └── TableExpandable.js
    │   ├── Tasks
    │   │   └── Tasks.js
    │   └── Typography
    │       ├── Danger.js
    │       ├── Info.js
    │       ├── Link.js
    │       ├── Muted.js
    │       ├── Primary.js
    │       ├── Quote.js
    │       ├── Success.js
    │       ├── Titulo1.js
    │       └── Warning.js
    ├── layouts
    │   ├── Admin.js
    │   ├── login.js
    │   └── RTL.js
    ├── variables
    │   ├── charts.js
    │   └── general.js
    └── views
        ├── Banner
        │   ├── FormBanners.js
        │   └── ListBanners.js
        ├── Categories
        │   └── List.js
        ├── Countries
        │   └── Countries.js
        ├── Cursos
        │   ├── FormCursos.js
        │   ├── FormSongs.js
        │   ├── ListCursos.js
        │   ├── RecomendedCourses.js
        │   ├── RewardsCourses.js
        │   └── TableStudents.js
        ├── Dashboard
        │   └── Dashboard.js
        ├── Mailing
        │   ├── MailHistory.js
        │   └── Mailing.js
        ├── Notifications
        │   └── Notifications.js
        ├── Scripts
        │   └── Scripts.js
        ├── Students
        │   ├── FormStudents.js
        │   ├── ListStudents.js
        │   └── TableSubscriptions.js
        └── Trash
            └── ListTrash.js
```

## Documentation of base project
- Documentation: https://demos.creative-tim.com/material-dashboard-react/#/documentation/tutorial

