/*!

=========================================================
* Material Dashboard React - v1.8.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-react
* Copyright 2019 Creative Tim (https://www.creative-tim.com)
* Licensed under MIT (https://github.com/creativetimofficial/material-dashboard-react/blob/master/LICENSE.md)

* Coded by Creative Tim

=========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

*/
// @material-ui/icons
import Person from "@material-ui/icons/Person";
//custom components TCDG
import ListCursos from "views/Cursos/ListCursos.js";
import FormCursos from "views/Cursos/FormCursos.js";
import FormSongs from "views/Cursos/FormSongs.js"
import CourseNavs from "components/CoursesNav/CourseNav.js"

import ListCategories from "views/Categories/List.js";

import ListStudents from "views/Students/ListStudents.js"
// core components/views for RTL layout
// import RTLPage from "views/RTLPage/RTLPage.js";
import FormStudents from "views/Students/FormStudents";
import ListTrash from "views/Trash/ListTrash.js";
import Countries from "views/Countries/Countries.js";
import Mailing from "views/Mailing/Mailing.js";
import MailHistory from "views/Mailing/MailHistory.js";
import Scripts from "views/Scripts/Scripts.js";
import Banner from "views/Banner/FormBanners.js";
import ListBanners from "views/Banner/ListBanners.js";

const dashboardRoutes = [
  {
    path: "/cursos",
    name: "Cursos",
    icon: Person,
    displayName:"Cursos y Canciones",
    component: ListCursos,
    layout: "/admin",
  },
  {
    path: "/curso-nuevo/:courseId?",
    name: "nuevo-curso",
    icon: Person,
    displayName:"Agregar nuevo curso.",
    component: FormCursos,
    layout: "/admin",
    subRoute:true
  },
  {
    path: "/song/:songId?",
    name: "song",
    icon: Person,
    displayName:"Agregar nueva canción.",
    component: FormSongs,
    nav:CourseNavs,
    layout: "/admin",
    subRoute:true
  },  
 
  {
    path: "/categories",
    name: "Categorias",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    displayName:"Categorías",
    component: ListCategories,
    layout: "/admin",
  },

  {
    path: "/Students",
    name: "Alumnos",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    displayName:"Alumnos",
    component: ListStudents,
    layout: "/admin",
  }
  ,

  {
    path: "/Student-new/:studentId?",
    name: "Crear Alumno",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    displayName:"Crear Alumno",
    component: FormStudents,
    layout: "/admin",
    subRoute: true
  },

  {
    path: "/mailing/",
    name: "Mailing",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    displayName:"Mailing y notificaciones",
    component: Mailing,
    layout: "/admin",
    subRoute: false
  },

  {
    path: "/history",
    name: "History",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    displayName:"Historial de Mails",
    component: MailHistory,
    layout: "/admin",
    subRoute: true
  },

  {
    path: "/countries/",
    name: "Precios",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    displayName:"Precios",
    component: Countries,
    layout: "/admin",
    subRoute: false
  },

  {
    path: "/scripts",
    name: "Scripts",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    displayName:"Scripts",
    component: Scripts,
    layout: "/admin",
  },

  {
    path: "/banners",
    name: "Banners",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    displayName:"Banners",
    component: ListBanners,
    layout: "/admin",
  },

  {
    path: "/banner/:bannerId?",
    name: "Banners",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    displayName:"Banners",
    component: Banner,
    layout: "/admin",
    subRoute: true

  },

  {
    path: "/trash",
    name: "Papelera",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    displayName:"Papelera",
    component: ListTrash,
    layout: "/admin",
  }
 
];

export default dashboardRoutes;

